<?php
/**
 * @file
 * This file includes page callbacks for Performance compare module.
 */

/**
 * The page callback for "module_analyzer/summary".
 */
function module_analyzer_summary_page() {
  $header = [
    t('Module name'),
    t('Type'),
    t('Required by'),
  ];

  $legend_header = [
    t('Icon'),
  ];

  $ok_icon = ['#theme' => 'image', '#path' => 'misc/message-24-ok.png'];
  $warning_icon = [
    '#theme' => 'image',
    '#path' => 'misc/message-24-warning.png',
  ];
  $info_icon = ['#theme' => 'image', '#path' => 'misc/message-24-info.png'];
  $legend_rows = [
    [0 => drupal_render($ok_icon)],
    [0 => drupal_render($info_icon)],
    [0 => drupal_render($warning_icon)],
  ];
  $legend_column = 1;

  $unused_modules = [];
  $plugins = module_analyzer_get_plugins();
  foreach ($plugins as $machine_name => $plugin) {
    $plugin_instance = ManalyzerManager::factory($plugin);
    $header[] = $plugin['plugin']['column'];


    $legend = $plugin_instance->getLegend();
    $legend_rows[0][$legend_column] = $legend[REQUIREMENT_OK];
    $legend_rows[1][$legend_column] = $legend[REQUIREMENT_INFO];
    $legend_rows[2][$legend_column] = $legend[REQUIREMENT_WARNING];
    $legend_header[] = $plugin['plugin']['column'];
    $legend_column++;

    if (!empty($plugin_instance)) {
      foreach ($plugin_instance->getSummary() as $name => $data) {
        $unused_modules[$name][$machine_name] = $data;
      }
    }
  }

  $modules = system_rebuild_module_data();

  $rows = [];
  foreach ($unused_modules as $module_name => $module_data) {
    $rows[$module_name][] = $module_name;
    $rows[$module_name]['type'] = module_analyzer_extract_module_type($modules[$module_name]->filename);
    $rows[$module_name]['required_by'] = module_analyzer_required_by_string($modules[$module_name]->required_by);
    $rows[$module_name]['weight'] = 0;

    foreach ($plugins as $plugin_name => $plugin) {
      if (isset($module_data[$plugin_name])) {
        $image = ['#theme' => 'image'];
        switch ($module_data[$plugin_name]['status']) {
          case REQUIREMENT_WARNING:
            $icon = 'warning';
            $weight = -1;
            break;

          case REQUIREMENT_INFO:
            $icon = 'info';
            $weight = 1;
            break;

          case REQUIREMENT_OK:
            $icon = 'ok';
            $weight = 10;
            break;

          default:
            $icon = 'error';
            $weight = -10;
        }

        // Possible values for $icon are: 'ok', 'warning', 'error', 'info'.
        $image['#path'] = 'misc/message-24-' . $icon . '.png';
        $rows[$module_name]['weight'] += $weight;
        $rows[$module_name][] = drupal_render($image);
      }
      else {
        $rows[$module_name][] = MODULE_ANALYZER_APP_EMPTY_CELL;
      }
    }
  }

  // Sort the set of modules before the render process.
  module_analyzer_sort_modules($rows);

  $table = [
    '#theme' => 'table',
    '#caption' => t('Summary: potentially unused modules - %count', ['%count' => count($rows)]),
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No content available.'),
  ];

  $content = [
    [
      '#theme' => 'table',
      '#caption' => t('Legend'),
      '#header' => $legend_header,
      '#rows' => $legend_rows,
    ],
    $table,
  ];

  return $content;
}

/**
 * Plugin's page callback.
 *
 * @param string $plugin_name
 *   Plugin name.
 *
 * @return array|string
 */
function module_analyzer_page_callback($plugin_name) {
  $content = '';
  ctools_include('plugins');

  $plugin = ctools_get_plugins('module_analyzer', 'manalyzer', $plugin_name);
  $plugin_instance = ManalyzerManager::factory($plugin);
  $data = $plugin_instance->getData();

  if (!empty($plugin['plugin']['render'])) {
    $function = 'module_analyzer_render_' . $plugin['plugin']['render'];
    if (function_exists($function)) {
      $content = $function($data);
    }
  }
  else {
    // Fallback if no handler found.
    module_load_include('inc', 'apps', 'apps.pages');
    $content = module_analyzer_status_report($data);
  }

  return $content;
}
