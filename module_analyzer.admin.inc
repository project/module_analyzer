<?php

/**
 * @file
 * Contains the config form for module_analyzer module.
 */

/**
 * Configuration form for module_analyzer module.
 *
 * @param array $form
 *   An array of elements and element properties.
 * @param array $form_state
 *   An array containing information about the current state of this form
 *   while a user is filling it out and submitting it. Importantly, this
 *   includes the values that the user may have entered into the form.
 *
 * @return array
 *   Returns Drupal form array.
 */
function module_analyzer_configure_form($form, &$form_state) {

  $form['dependencies'] = [
    '#type' => 'fieldset',
    '#title' => t('Settings for Dependencies plugin'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['dependencies']['module_analyzer_exclude_profile'] = [
    '#type' => 'checkbox',
    '#title' => t('Ignore dependencies in the installation profile'),
    '#description' => t('Do not consider module dependencies defined in the installation profile.'),
    '#default_value' => variable_get('module_analyzer_exclude_profile', FALSE),
  ];

  $form['dependencies']['module_analyzer_exclude_profile_name'] = [
    '#type' => 'textfield',
    '#title' => t('Installation Profile name'),
    '#default_value' => variable_get('module_analyzer_exclude_profile_name', []),
    '#states' => [
      'invisible' => [
        ':input[name="module_analyzer_exclude_profile"]' => ['checked' => FALSE],
      ],
    ],
  ];

  $form['dependencies']['module_analyzer_exclude_features'] = [
    '#type' => 'checkbox',
    '#title' => t('Do not show Features modules in the unused modules list'),
    '#description' => t('Ignore features modules and do not show them as candidates for disabling.'),
    '#default_value' => variable_get('module_analyzer_exclude_features', FALSE),
  ];

  $modules = module_list();
  asort($modules);
  $form['exlcude_modules'] = [
    '#type' => 'fieldset',
    '#title' => t('Exclude modules from the analysis'),
  ];
  $form['exlcude_modules']['module_analyzer_enable_exclude'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#default_value' => variable_get('module_analyzer_enable_exclude'),
    '#description' => t('Exclude specific modules from the analysis.'),
  ];
  $form['exlcude_modules']['module_analyzer_exclude_modules'] = [
    '#type' => 'select',
    '#title' => t('Modules'),
    '#multiple' => TRUE,
    '#options' => $modules,
    '#default_value' => variable_get('module_analyzer_exclude_modules', []),
    '#disabled' => !variable_get('module_analyzer_enable_exclude'),
  ];
  $form['module_analyzer_refresh_dbstate_analysis'] = [
    '#type' => 'submit',
    '#value' => t('Re-run database analysis'),
    '#submit' => ['module_analyzer_configure_rerun_db_analysis'],
  ];

  return system_settings_form($form);
}

/**
 * Flush service variables for rerun DB analysis.
 */
function module_analyzer_configure_rerun_db_analysis() {
  variable_del('module_analyzer_dbstate_schema_cache');
  variable_del('module_analyzer_dbstate_variables_cache');
}
