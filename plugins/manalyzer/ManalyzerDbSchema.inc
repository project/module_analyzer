<?php

/**
 * @file
 * Contains ManalyzerDbSchema class.
 */

$plugin = [
  'name' => 'Schema',
  'handler' => [
    'class' => 'ManalyzerDbSchema',
  ],
  'plugin' => [
    'name' => t('Database state: schema.', [], module_analyzer_get_context()),
    'description' => t("Provides the information about database tables' state.", [], module_analyzer_get_context()),
    'menu_item' => 'db-schema',
    'column' => 'Schema',
  ],
  'weight' => 1,
];

/**
 * Class ManalyzerDbSchema.
 */
class ManalyzerDbSchema implements ManalyzerInterface {

  /**
   * App status message items.
   */
  private $items = [];

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function getData() {
    $modules = module_list();

    if (!variable_get('module_analyzer_dbstate_schema_cache')) {
      $this->analyzeModuleSchemaStatus($modules);
      variable_set('module_analyzer_dbstate_schema_cache', $this->items);
    }
    else {
      return variable_get('module_analyzer_dbstate_schema_cache');
    }

    return $this->items;
  }

  /**
   * The callback function with used to export summary results to a page.
   */
  public function getSummary() {
    $summary = [];
    $items = $this->getData();
    foreach ($items as $module_name => $item) {
      $summary[$module_name] = [
        'status' => $item['severity'],
        'description' => t('Description'),
        'module_info' => $item['title'],
      ];
    }

    return $summary;
  }

  /**
   * The callback function with used to export results to the app status page.
   */
  public function getStatus() {
    return [
      [
        'severity' => REQUIREMENT_INFO,
        'title' => t('Audit: database schema'),
        'description' => '',
        'action' => [
          l(t("Plugin's page"), 'admin/apps/development/module_analyzer/db-schema'),
        ],
      ],
    ];
  }

  /**
   * Analyzes modules' schema status.
   *
   * @param array $modules
   *   List of modules being processed.
   */
  private function analyzeModuleSchemaStatus($modules) {

    foreach ($modules as $module_name => $module) {
      if (in_array($module_name, $this->getExcludedModules())) {
        // Exclude some of the "must have" modules.
        continue;
      }

      $schemas = drupal_get_schema_unprocessed($module_name);
      if (!$schemas) {
        continue;
      }

      $module_info = system_get_info('module', $module_name);
      $empty_tables = [];
      foreach ($schemas as $table => $schema) {
        if (!db_table_exists($table)) {
          continue;
        }

        if (substr($table, 0, 6) == 'cache_') {
          continue;
        }

        $table_is_empty = !db_query('SELECT NULL FROM {' . $table . '} LIMIT 1')->rowCount();
        if (!$table_is_empty) {
          $this->items[$module_name] = [
            'severity' => REQUIREMENT_WARNING,
            'title' => t('Module %module', ['%module' => $module_info['name']]),
            'description' => t('Some of the declared (via hook_schema) database tables are not empty.'),
            'action' => [t('n/a')],
          ];

          continue 2;
        }

        $empty_tables[] = $table;
      }

      if (!$empty_tables) {
        continue;
      }

      $this->items[$module_name] = [
        'severity' => REQUIREMENT_OK,
        'title' => t('Module %module', ['%module' => $module_info['name']]),
        'description' => t('All declared (via hook_schema) database tables are empty: %tables.', ['%tables' => implode(', ', $empty_tables)]),
        'action' => [t('n/a')],
      ];
    }
  }

  /**
   * Returns list of excluded database table names.
   *
   * @return array
   *   list of database table names
   */
  private function getExcludedModules() {
    return array_merge(
      [
        'views',
        'panelizer',
        'ctools',
        'user',
        'system',
        'panels',
        'features',
        'menu',
        'taxonomy',
        'token',
        'node',
        'field',
        'field_sql_storage',
        'image',
        'module_analyzer',
      ],
      array_keys(variable_get('module_analyzer_exclude_modules', []))
    );
  }

  /**
   * Define the legend for the results table.
   */
  public function getLegend() {
    return [
      REQUIREMENT_OK => t('The module assumed as potentially unused one since all declared (via %hook hook) database tables  (except those prefixed with %prefix) are empty.', [
        '%prefix' => 'cache_',
        '%hook' => 'hook_schema',
      ]),
      REQUIREMENT_WARNING => t('At least one database table has a record for the module.'),
      REQUIREMENT_INFO => '',
    ];
  }

}
