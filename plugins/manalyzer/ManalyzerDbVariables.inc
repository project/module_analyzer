<?php

/**
 * @file
 * Contains ManalyzerDbVariables class.
 */

$plugin = [
  'name' => 'Variables',
  'handler' => [
    'class' => 'ManalyzerDbVariables',
  ],
  'plugin' => [
    'name' => t('Database state: variables.', [], module_analyzer_get_context()),
    'description' => t("Provides the information about variables' state.", [], module_analyzer_get_context()),
    'menu_item' => 'db-variables',
    'column' => 'Variables',
  ],
  'weight' => 1,
];

/**
 * Class ManalyzerDbVariables.
 */
class ManalyzerDbVariables implements ManalyzerInterface {

  /**
   * @var array App status message items.
   */
  private $items = [];

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function getData() {
    $modules = module_list();

    if (!variable_get('module_analyzer_dbstate_variables_cache')) {
      $this->analyzeVariables($modules);
      variable_set('module_analyzer_dbstate_variables_cache', $this->items);
    }
    else {
      return variable_get('module_analyzer_dbstate_variables_cache');
    }

    return $this->items;
  }

  /**
   * The callback function with used to export summary results to a page.
   */
  public function getSummary() {
    $items = $this->getData();
    foreach ($items as $module_name => $item) {
      $summary[$module_name] = [
        'status' => $item['severity'],
        'description' => t('Description'),
        'module_info' => $item['title'],
      ];
    }

    return $summary;
  }

  /**
   * Analyzes modules' status of variables.
   *
   * @param array $modules
   *   List of modules being processed.
   */
  private function analyzeVariables($modules) {
    foreach ($modules as $module_name => $module) {
      if (in_array($module_name, $this->getExcludedModules())) {
        continue;
      }

      $files = file_scan_directory(drupal_get_path('module', $module_name), '/\.install|.inc|.module$/');
      $variables = [];
      foreach ($files as $uri => $file) {
        $file_content = file_get_contents($uri);
        $matches = [];
        preg_match_all("/variable_set\('([a-zA-Z0-9_]+)', ([\'a-zA-Z0-9_]+)\);/", $file_content, $matches);

        if (empty($matches[1])) {
          continue;
        }

        $module_info = system_get_info('module', $module_name);
        foreach ($matches[1] as $key => $variable_name) {
          $variable_value = $matches[2][$key];

          if (!$this->validateVariableValue($variable_value)) {
            continue;
          }

          if (!variable_get($variable_name, $variable_value) === $variable_value) {
            // Skip this module since at least one variable is in the overridden
            // state.
            $this->items[$module_name] = [
              'severity' => REQUIREMENT_WARNING,
              'title' => t('Module %module', ['%module' => $module_info['name']]),
              'description' => t('One (or more) declared (via variable_set function calls) variable is overridden.'),
              'action' => [t('n/a')],
            ];
            continue 3;
          }

          $variables[] = $variable_name;
        }
      }

      if (empty($variables)) {
        continue;
      }

      $this->items[$module_name] = [
        'severity' => REQUIREMENT_OK,
        'title' => t('Module %module', ['%module' => $module_info['name']]),
        'description' => t('All declared (via variable_set function calls) variables have default values: %variables.', ['%variables' => implode(', ', $variables)]),
        'action' => [t('n/a')],
      ];
    }
  }

  /**
   * Returns list of excluded database table names.
   *
   * @return array
   *   list of database table names
   */
  private function getExcludedModules() {
    return array_merge(
      [
        'views',
        'panelizer',
        'ctools',
        'user',
        'system',
        'panels',
        'features',
        'menu',
        'taxonomy',
        'token',
        'node',
        'field',
        'field_sql_storage',
        'image',
        'module_analyzer',
        'jjbos',
      ],
      array_keys(variable_get('module_analyzer_exclude_modules', []))
    );
  }

  /**
   * Validates a variable's value.
   *
   * @param string $value
   *   Value of the variable.
   *
   * @return bool
   *   whether value is valid or not
   */
  private function validateVariableValue(&$value) {
    if ('true' == $value || 'TRUE' == $value) {
      $value = TRUE;

      return TRUE;
    }

    if ('false' == $value || 'FALSE' == $value) {
      $value = FALSE;
      return TRUE;
    }

    if (substr($value, 0, 1) == "'" && substr($value, -1) == "'") {
      $value = str_replace("'", '', $value);
      return TRUE;
    }

    if (preg_match('/^[0-9]+$/', $value)) {
      $value = (int) $value;

      return TRUE;
    }


    if (defined($value)) {
      $value = constant($value);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * The callback function with used to export results to the app status page.
   */
  public function getStatus() {
    return [
      [
        'severity' => REQUIREMENT_INFO,
        'title' => t('Audit: variables'),
        'description' => '',
        'action' => [
          l(t("Plugin's page"), 'admin/apps/development/module_analyzer/db-variables'),
        ],
      ],
    ];
  }

  /**
   * Define the legend for the results table.
   */
  public function getLegend() {
    return [
      REQUIREMENT_OK => t('The module assumed as potentially unused one since all declared (via %func function) variables have default values.', ['%func' => 'variable_set']),
      REQUIREMENT_WARNING => t('At least one variable has non-default (overridden) value for the module.'),
      REQUIREMENT_INFO => '',
    ];
  }

}
