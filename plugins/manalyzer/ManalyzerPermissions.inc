<?php

/**
 * @file
 * Contains ManalyzerPermissions class.
 */

$plugin = [
  'name' => 'Permissions',
  'handler' => [
    'class' => 'ManalyzerPermissions',
  ],
  'plugin' => [
    'name' => t('Permissions.', [], module_analyzer_get_context()),
    'description' => t('Provides the information about permissions in use.', [], module_analyzer_get_context()),
    'menu_item' => 'permissions',
    'column' => 'Permissions',
  ],
  'weight' => 1,
];

/**
 * Class ManalyzerPermissions.
 */
class ManalyzerPermissions implements ManalyzerInterface {

  /**
   * @var array App status message items.
   */
  private $items = [];

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function getData() {
    $modules = module_list();
    $this->analyzePermissions($modules);

    return $this->items;
  }

  /**
   * The callback function with used to export summary results to a page.
   */
  public function getSummary() {
    $items = $this->getData();
    foreach ($items as $module_name => $item) {
      $summary[$module_name] = [
        'status' => $item['severity'],
        'description' => t('Description'),
        'module_info' => $item['title'],
      ];
    }

    return $summary;
  }

  /**
   * Analyzes permission set for each module.
   *
   * @param array $modules
   *   List of modules being processed.
   */
  private function analyzePermissions($modules) {
    $administrator_role = user_role_load_by_name('administrator');
    $role_id = $administrator_role->rid;

    // Get list of modules for which at least one permission is set to
    // non-administrator role.
    $q = db_select('role_permission', 'p')
      ->fields('p', ['module'])
      ->condition('p.rid', $role_id, '<>')
      ->condition('p.module', $modules, 'IN')
      ->groupBy('p.module')
      ->execute();
    $modules_in_use = $q->fetchCol();

    foreach ($modules as $module_name => $module) {
      if (in_array($module_name, $this->getExcludedModules())) {
        // Exclude some of the "must have" modules.
        continue;
      }

      $defined_permissions = module_invoke($module_name, 'permission');
      if (!$defined_permissions) {
        continue;
      }

      $module_info = system_get_info('module', $module_name);
      if (in_array($module_name, $modules_in_use)) {
        $this->items[$module_name] = [
          'severity' => REQUIREMENT_WARNING,
          'title' => t('Module %module', ['%module' => $module_info['name']]),
          'description' => t('Set of permissions are not in the default state for %module module.', ['%module' => $module_info['name']]),
        ];
        continue;
      }

      $this->items[$module_name] = [
        'severity' => REQUIREMENT_OK,
        'title' => t('Module %module', ['%module' => $module_info['name']]),
        'description' => t('Set of permissions are in the default state for %module module.', ['%module' => $module_info['name']]),
      ];
    }
  }

  /**
   * Returns list of excluded database table names.
   *
   * @return array
   *   list of database table names
   */
  private function getExcludedModules() {
    return array_merge(
      [
        'views',
        'panelizer',
        'ctools',
        'user',
        'system',
        'panels',
        'features',
        'menu',
        'taxonomy',
        'token',
        'node',
        'field',
        'field_sql_storage',
        'image',
        'module_analyzer',
        'jjbos',
      ],
      array_keys(variable_get('module_analyzer_exclude_modules', []))
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function getStructure() {
    $structure['name'] = 'db_tables';
    $structure['label'] = 'DB tables';

    $structure['properties'] = [
      'name' => ['type' => 'string'],
      'status' => ['type' => 'string'],
      'version' => ['type' => 'string'],
      'state' => ['type' => 'string'],
    ];

    return $structure;
  }

  /**
   * The callback function with used to export results to the app status page.
   */
  public function getStatus() {
    return [
      [
        'severity' => REQUIREMENT_INFO,
        'title' => t('Audit: permissions'),
        'description' => '',
        'action' => [
          l(t("Plugin's page"), 'admin/apps/development/module_analyzer/permissions'),
        ],
      ],
    ];
  }

  /**
   * Define the legend for the results table.
   */
  public function getLegend() {
    return [
      REQUIREMENT_OK => t('The module assumed as potentially unused one since the set of permissions is in the default state (granted only for %role role).', ['%role' => 'administrator']),
      REQUIREMENT_WARNING => t('At least one permission is granted for a non-admin role for the module.'),
      REQUIREMENT_INFO => '',
    ];
  }

}
