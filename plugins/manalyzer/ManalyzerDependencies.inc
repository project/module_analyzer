<?php

/**
 * @file
 * Contains ManalyzerDependencies class.
 */

$plugin = [
  'name' => 'Dependencies',
  'handler' => [
    'class' => 'ManalyzerDependencies',
  ],
  'plugin' => [
    'name' => t('Site Features status', [], module_analyzer_get_context()),
    'description' => t('Provides the information about dependencies between modules', [], module_analyzer_get_context()),
    'render' => 'dependencies_table',
    'column' => 'Dependencies',
    'menu_item' => 'dependencies',
  ],
  'weight' => 1,
];

/**
 * Class ManalyzerDependencies.
 *
 * Allows us to analyze dependencies between modules.
 */
class ManalyzerDependencies implements ManalyzerInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function getData() {
    return $this->getRequiredModules();
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function getSummary() {
    $data_rows = $this->getRequiredModules();
    foreach ($data_rows as $step => $modules) {
      foreach ($modules as $name => $module) {
        $summary[$name] = [
          'status' => ($step == 1) ? REQUIREMENT_OK : REQUIREMENT_INFO,
          'description' => t('Step - %step', ['%step' => $step]),
          'module_info' => $module,
        ];
      }
    }

    return $summary;
  }

  /**
   * Returns required modules.
   */
  private function getRequiredModules($modules = FALSE, $step = 1, &$unused_modules = []) {
    if (!$modules) {
      $modules = system_rebuild_module_data();
    }

    foreach ($modules as $module) {
      if ($module->status == 1 && !$this->checkExcludedModules($module)) {
        if (!$this->moduleRequiredBy($module)) {
          $unused_modules[$step][$module->name] = $module;
          unset($modules[$module->name]);
        }
      }
    }

    // Remove potentially disabled modules from "required_by".
    if (!empty($unused_modules[$step])) {
      $this->requiredByCleanup($modules, $unused_modules[$step]);
      $this->unusedModulesSort($unused_modules[$step]);

      if ($step !== FALSE) {
        $step++;
        $this->getRequiredModules($modules, $step, $unused_modules);
      }
    }

    return $unused_modules;
  }

  /**
   * Remove modules from "Required_by" section.
   */
  private function requiredByCleanup(& $modules, $unused_modules) {
    foreach ($modules as $key => $module) {
      foreach ($module->required_by as $name => $child) {
        if (isset($unused_modules[$name])) {
          // Delete the module from dependencies.
          unset($modules[$key]->required_by[$name]);
          $modules[$key]->released_with[] = $name;
        }
      }
    }
  }

  /**
   * Sort unused modules by type.
   */
  private function unusedModulesSort(& $unused_modules) {
    uasort(
      $unused_modules,
      function ($a, $b) {
        $weight = [
          'core' => 10,
          'contrib' => 5,
          'custom' => 1,
          'features' => 0,
        ];

        $type_a = $weight[module_analyzer_extract_module_type($a->filename)];
        $type_b = $weight[module_analyzer_extract_module_type($b->filename)];
        return $type_a < $type_b;
      }
    );
  }

  /**
   * Returns required modules.
   */
  private function checkExcludedModules($module) {
    $module_name = $module->name;
    $excluded_modules[] = drupal_get_profile();

    $excluded = FALSE;
    if (in_array($module_name, $excluded_modules)) {
      $excluded = TRUE;
    }

    $ignore_features = variable_get('module_analyzer_exclude_features', FALSE);
    if ($excluded == FALSE && $ignore_features && isset($module->info['features']['features_api'])) {
      $excluded = TRUE;
    }

    return $excluded;
  }

  /**
   * Checks if the module is really required by ancestors.
   *
   * @param string $module
   *   The module name.
   *
   * @return bool
   */
  private function moduleRequiredBy($module) {
    $required = FALSE;
    $profile_name = drupal_get_profile();

    $ignore_profile = variable_get('module_analyzer_exclude_profile', FALSE);
    foreach ($module->required_by as $name => $child) {
      if ($ignore_profile && $name == $profile_name) {
        continue;
      }

      // If the module is required by Drupal itself.
      if (module_exists($name) || !empty($module->info['required'])) {
        $required = TRUE;
        break;
      }
    }

    return $required;
  }

  /**
   * The callback function with used to export results to the app status page.
   */
  public function getStatus() {
    return [
      [
        'severity' => REQUIREMENT_INFO,
        'title' => t('Audit: module dependencies'),
        'description' => '',
      ],
    ];
  }

  /**
   * Define the legend for the results table.
   */
  public function getLegend() {
    // Add a required file that contains file with constants.
    include_once DRUPAL_ROOT . '/includes/install.inc';

    return [
      REQUIREMENT_OK => t('The module assumed as potentially unused.'),
      REQUIREMENT_WARNING => 'The module assumed as potentially used.',
      REQUIREMENT_INFO => t('Some specific information available for the module.'),
    ];
  }

}
