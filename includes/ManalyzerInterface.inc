<?php

/**
 * @file
 * Contains ManalyzerInterface interface.
 */

/**
 * The interface for all plugins which helps to identify unused modules.
 */
interface ManalyzerInterface {

  /**
   * Get a data provided by the plugin.
   */
  public function getData();

  /**
   * Get the general status or/and results.
   */
  public function getStatus();

  /**
   * Get the set of unused modules in order to provide common statistics.
   */
  public function getSummary();

  /**
   * Get the legend info.
   */
  public function getLegend();

}
