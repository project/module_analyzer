<?php

/**
 * @file
 * Contains ManalyzerManager class.
 */

/**
 * The class for managing manalyzer plugins.
 */
class ManalyzerManager {

  /**
   * Basic factory function for report plugins.
   */
  public static function factory($plugin_name) {
    $ctools_plugin = FALSE;

    // Make sure that the handler class exists and that it has
    // this class as one of its parents.
    if (class_exists($plugin_name['handler']['class'])) {
      $ctools_plugin = new $plugin_name['handler']['class']($plugin_name);
    }
    else {
      // Can not find the plugin handler class.
      watchdog('module_analyzer', 'Class %name is not exists!', array('%name' => $plugin_name['handler']['class']), WATCHDOG_ERROR);
    }

    return $ctools_plugin;
  }

  /**
   * Basic factory function for report plugins.
   */
  public static function convertData($data) {
    return $data;
  }

}
